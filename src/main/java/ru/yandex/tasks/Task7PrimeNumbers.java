// Extra task https://leetcode.com/problems/prime-in-diagonal/

package ru.yandex.tasks;

import java.util.ArrayList;
import java.util.Arrays;

public class Task7PrimeNumbers {
    public static int[] findPrimes (int N) {
        /*
         * 2 <= N <= 10^6
         * Выход: отсортированный массив всех простых чисел от 2 до N
         */
        // (ﾉ◕ヮ◕)ﾉ*:･ﾟ✧ WRITE CODE HERE (ﾉ◕ヮ◕)ﾉ*:･ﾟ✧
        var res = new ArrayList<Integer>();
        boolean[] dp = new boolean[N + 1];
        for (int i = 2; i * i <= N; i++) {
            if (dp[i]) {
                continue;
            }
            res.add(i);
            for (int j = i; j <= N; j += i) {
                dp[j] = true;
            }
        }
        for (int i = (int)(Math.sqrt(N) - 1); i <= N ; i++) {
            if (!dp[i]) {
                res.add(i);
            }
        }
        int[] arr = new int[res.size()];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = res.get(i);
        }
        return arr;
    }

    public static void selfCheck() {
        int[] output = {2, 3, 5};

        assert (Arrays.equals(output, findPrimes(5)));
    }

    public static void main(String[] args) {
        selfCheck();
    }
}
