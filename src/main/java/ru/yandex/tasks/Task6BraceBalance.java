// Extra task https://leetcode.com/problems/valid-parentheses/

package ru.yandex.tasks;

import java.util.ArrayDeque;
import java.util.Map;

public class Task6BraceBalance {
    public static boolean checkBalance(String sequence) {
        /*
         * sequence - последовательность скобок []{}() длины до 10^5
         * Выход: true/false, является ли строка ПСП
         */
        // (ﾉ◕ヮ◕)ﾉ*:･ﾟ✧ WRITE CODE HERE (ﾉ◕ヮ◕)ﾉ*:･ﾟ✧
        var stack = new ArrayDeque<Character>();
        var map = Map.of(
                ')', '(',
                '}', '{',
                ']', '['
        );

        char[] chars = sequence.toCharArray();

        for (char c : chars) {
            if (map.containsKey(c)) {
                if (stack.isEmpty() || (char) stack.getLast() != map.get(c)) {
                    return false;
                }
                stack.removeLast();
            } else {
                stack.addLast(c);
            }
        }
        return stack.isEmpty();
    }

    public static void selfCheck() {
        String test1 = "[({})]{[]}";
        String test2 = "{({})}{";

        assert checkBalance(test1);
        assert !checkBalance(test2);
    }

    public static void main(String[] args) {
        selfCheck();
    }
}
