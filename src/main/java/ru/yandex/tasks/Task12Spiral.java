package ru.yandex.tasks;

import java.util.Arrays;

public class Task12Spiral {
    public static int[][] draw (int N) {
        /*
         * Отрисовывает "спираль" в двумерном массиве по часовой стрелке
         * 1 <= N <= 10^3
         * Выход: массив со спиралью
         */
        // (ﾉ◕ヮ◕)ﾉ*:･ﾟ✧ WRITE CODE HERE (ﾉ◕ヮ◕)ﾉ*:･ﾟ✧
        int[][] matrix = new int[N][N];
        int cnt = 0;
        int left = 0;
        int right = N - 1;
        int up = 0;
        int down = N -1;

        while (cnt < N * N) {
            for (int i = left; i <= right && cnt < N * N; i++) {
                matrix[up][i] = ++cnt;
            }
            for (int i = up + 1; i <= down && cnt < N * N; i++) {
                matrix[i][right] = ++cnt;
            }
            for (int i = right - 1; i >= left; i--) {
                matrix[down][i] = ++cnt;
            }
            for (int i = down - 1; i > up; i--) {
                matrix[i][left] = ++cnt;
            }
            left++;
            right--;
            up++;
            down--;
        }
        return matrix;
    }

    public static void selfCheck() {
        int[][] output = {
                {1, 2, 3, 4, 5, 6, 7},
                {24, 25, 26, 27, 28, 29, 8},
                {23, 40, 41, 42, 43, 30, 9},
                {22, 39, 48, 49, 44, 31, 10},
                {21, 38, 47, 46, 45, 32, 11},
                {20, 37, 36, 35, 34, 33, 12},
                {19, 18, 17, 16, 15, 14, 13}
        };

        assert Arrays.deepEquals(output, draw(7));
    }

    public static void main(String[] args) {
        selfCheck();
    }
}
