package ru.yandex.tasks;

import java.net.Inet4Address;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Task10Permutations {
    public static int[][] permutations(int[] numbers) {
        List<int[]> res = new ArrayList<>();

        helper(numbers, res, 0);
        var ans = res.toArray(new int[0][]);
        Arrays.sort(ans, Arrays::compare);
        return ans;
    }

    private static void helper(int[] nums, List<int[]> res, int start) {
        if (start == nums.length) {
            int[] ans = new int[start];
            for (int i = 0; i < ans.length; i++) {
                ans[i] = nums[i];
            }
            res.add(ans);
            return;
        }

        for (int i = start; i < nums.length; i++) {
            swap(nums, start, i);
            helper(nums, res, start + 1);
            swap(nums, start, i);
        }
    }

    private static void swap(int[] nums, int i, int j) {
        int temp = nums[i];
        nums[i] = nums[j];
        nums[j] = temp;
    }

    public static void selfCheck() {
        int[] input = {1, 2, 3};
        int[][] output = {
                {1, 2, 3},
                {1, 3, 2},
                {2, 1, 3},
                {2, 3, 1},
                {3, 1, 2},
                {3, 2, 1}
        };

        assert Arrays.deepEquals(output, permutations(input));
    }

    public static void main(String[] args) {
        selfCheck();
    }
}
