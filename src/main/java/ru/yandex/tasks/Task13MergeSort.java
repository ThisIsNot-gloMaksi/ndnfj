package ru.yandex.tasks;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Task13MergeSort {
    public static List<Integer> sort(List<Integer> numbers) {
        if (numbers.size() < 2) {
            return numbers;
        }
        int mid = numbers.size() / 2;
        var left = sort(numbers.subList(0, mid));
        var right = sort(numbers.subList(mid, numbers.size()));
        return merge(left, right);
    }

    static ArrayList<Integer> merge(List<Integer> nums1, List<Integer> nums2) {
        int p1 = 0;
        int p2 = 0;
        var res = new ArrayList<Integer>();

        while (p1 < nums1.size() || p2 < nums2.size()) {
            if (p2 == nums2.size() || (p1 < nums1.size() && (int)nums1.get(p1) < nums2.get(p2))) {
                res.add(nums1.get(p1++));
            } else {
                res.add(nums2.get(p2++));
            }
        }
        return res;
    }


    public static void selfCheck() {
        ArrayList<Integer> input = new ArrayList<>(Arrays.asList(3, 4, 5, 2, 1));
        ArrayList<Integer> output = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5));

        assert output.equals(sort(input));
    }

    public static void main(String[] args) {
        selfCheck();
    }
}
