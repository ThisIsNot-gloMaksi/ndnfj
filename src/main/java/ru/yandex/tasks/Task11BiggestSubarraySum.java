package ru.yandex.tasks;

public class Task11BiggestSubarraySum {

    public static Subarray find(int[] numbers) {
        /*
         * Находит подмассив (подряд идущие элементы массива) с наибольшей суммой элементов
         * numbers: массив целых чисел, -10^5 <= numbers[i] <= 10^5, длина массива до 10^5
         * Выход: Subarray
         */
        // (ﾉ◕ヮ◕)ﾉ*:･ﾟ✧ WRITE CODE HERE (ﾉ◕ヮ◕)ﾉ*:･ﾟ✧
        if (numbers == null) {
            return null;
        }
        if (numbers.length == 0) {
            Subarray res = new Subarray();
            res.left = 0;
            res.right = 0;
            return res;
        }
        Subarray res = new Subarray();
        res.left = 0;
        res.right = 1;

        Subarray curS = new Subarray();
        curS.left = 0;
        curS.right = 1;

        int max = numbers[0];
        int cur = numbers[0];

        for (int i = 1; i < numbers.length; i++) {
            if (cur + numbers[i] < numbers[i]) {
                curS.left = i;
                curS.right = i + 1;

                cur = numbers[i];
            } else {
                cur += numbers[i];
                curS.right++;
            }

            if (max < cur) {
                max = cur;
                res.right = curS.right;
                res.left = curS.left;
            }
        }
        return res;
    }

    public static void selfCheck() {
        int[] output = {1, 2, -5, 7, -1, 6};
        Subarray ans = find(output);

        assert ans != null;
        assert (ans.left == 3 && ans.right == 6);
    }

    public static void main(String[] args) {
        selfCheck();
    }
}
